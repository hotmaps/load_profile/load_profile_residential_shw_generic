# generic typical day hourly profiles dependent on temperature and country for domestic hot water demand in the residential sector




## Repository structure
Files:
```
data/hotmaps_task_2.7_load_profile_residential_shw_generic.csv               -- contains the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation


This dataset provides the hourly heat demand for domestic hot water in the residential sector for typical days with different temperature levels for the EU-27, UK+CH+LI. As CH + LI are not part of the EU, they are not specifically modeled. We, thus, used the generic profiles of AT for both countries.
The profiles can be used to assemble a yearlong demand profile for a NUTS2 region, if the daily average temperature for the specific region and year is available. 

Generic files are supposed to enable the user to produce load profiles of his own using own data and a structure year of her/his own choice. For sanitary hot water demand we assume that demand is independent from outside temperature, but depends on seasonal influences and the type of day in a week. 
For the load profiles for hot water demand, we provided a yearlong profile for the year 2010 (in which the typedays are set in the order of this year). However, we want to give the user the opportunity to use a structure year of his/her choice.
Structure year in this context means the order of days in the course of the year. The columns “day type” refers to the type of a day in the week:
- weekdays = typeday 0;
- saturday or day before a holiday = typeday 1;
- sunday or holiday = typeday 2
To integrate a seasonal influence into the demand profile, the column “season” is used.
- 0 = Summer (15/05 - 14/09)
- 1 = Winter (1/11 - 20/3)
- 2 = Transition (21/3 - 14/5 & 15/9 - 31/10)
Yearlong profiles can be generated from the generic profiles provided here following the following steps:
- determining the structure year for which the profiles are generated
- ordering the typeday/season tuples according to the selected year 
- allocating the respective load value for the typeday/season tuple to each hour
- scaling the total sum of the annual yearlong profile (i.e. the integral of the profile) according to the annual total demand

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8 page 121ff.


## Limitations of the datasets
The datasets provided have to be interpreted as simplified indicator to enable the analysis that can be carried out within the Hotmaps toolbox. It should be noted that the results of the toolbox can be improved with recorded heat demand data of local representatives of the tertiary sector.

## References
[1] [Harmonised European time use surveys](http://ec.europa.eu/eurostat/web/products-manuals-and-guidelines/-/KS-RA-08-014) Eurostat, 2009, checked on 2/1/2018.


## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kuehnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License


Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
